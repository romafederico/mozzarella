class BukBookmarks(models.Model):
    story_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    creation_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'buk_bookmarks'


class BukChapters(models.Model):
    chapter_id = models.AutoField(primary_key=True)
    story_id = models.IntegerField(blank=True, null=True)
    chapter_number = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    html = models.IntegerField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'buk_chapters'


class BukFollowers(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    follower_id = models.IntegerField(blank=True, null=True)
    follow_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'buk_followers'


class BukReviews(models.Model):
    review_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField()
    story_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'buk_reviews'


class BukStories(models.Model):
    story_id = models.AutoField(primary_key=True)
    update_date = models.DateTimeField()
    user_id = models.IntegerField(blank=True, null=True)
    username = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    summary = models.CharField(max_length=3000, blank=True, null=True)
    keywords = models.CharField(max_length=255, blank=True, null=True)
    image = models.CharField(max_length=150, blank=True, null=True)
    creation_date = models.DateField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'buk_stories'


class BukUsers(models.Model):
    user_id = models.AutoField(primary_key=True)
    update_date = models.DateTimeField()
    email = models.CharField(max_length=150, blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    facebook_id = models.CharField(unique=True, max_length=150, blank=True, null=True)
    firstname = models.TextField(blank=True, null=True)
    lastname = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    avatar = models.CharField(max_length=150, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)
    facebook_avatar = models.CharField(max_length=3000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'buk_users'


class StatsDownloads(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    story_id = models.IntegerField(blank=True, null=True)
    format = models.IntegerField(blank=True, null=True)
    download_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'stats_downloads'


class StatsLogins(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    login_date = models.DateTimeField()
    ip = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stats_logins'


class StatsReads(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    story_id = models.IntegerField(blank=True, null=True)
    referer = models.CharField(max_length=200, blank=True, null=True)
    read_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'stats_reads'


class StatsVotes(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    story_id = models.IntegerField(blank=True, null=True)
    value = models.TextField(blank=True, null=True)
    vote_date = models.DateTimeField()
    ip = models.CharField(max_length=255, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stats_votes'

from django.contrib import admin
from .models.stories import Story
from .models.users import User

admin.site.register(Story)
admin.site.register(User)
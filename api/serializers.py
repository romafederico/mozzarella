from rest_framework import serializers
from .models.stories import Story
from .models.users import User
from .models.chapters import Chapter
from .models.signups import Signup
from .models.logins import Login
from .models.reads import Read


class UsersSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'


class StorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Story
        fields = '__all__'


class ChaptersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Chapter
        fields = '__all__'


class SignupsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Signup
        fields = '__all__'


class LoginsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Login
        fields = '__all__'


class ReadsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Read
        fields = '__all__'

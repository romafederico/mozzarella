from django.db import models


class Story(models.Model):
    story_id = models.IntegerField(primary_key=True)
    update_date = models.DateTimeField()
    user_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    username = models.CharField(max_length=150, blank=True, null=True)
    summary = models.CharField(max_length=3000, blank=True, null=True)
    keywords = models.CharField(max_length=255, blank=True, null=True)
    image = models.CharField(max_length=150, blank=True, null=True)
    creation_date = models.DateField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        managed = False
        db_table = 'buk_stories'


from django.db import models


class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    update_date = models.DateTimeField(auto_now=True, editable=False, null=False, blank=False)
    email = models.CharField(max_length=150, blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    facebook_id = models.TextField(blank=True, null=True)
    firstname = models.TextField(blank=True, null=True)
    lastname = models.TextField(blank=True, null=True)
    username = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    avatar = models.CharField(max_length=150, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)
    facebook_avatar = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.username

    class Meta:
        managed = False
        db_table = 'buk_users'

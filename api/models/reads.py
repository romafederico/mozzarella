from django.db import models


class Read(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    story_id = models.IntegerField(blank=True, null=True)
    referer = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stats_reads'
from django.db import models


class Chapter(models.Model):
    chapter_id = models.IntegerField(primary_key=True)
    story_id = models.IntegerField(blank=True, null=True)
    chapter_number = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    html = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'buk_chapters'

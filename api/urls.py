from django.conf.urls import url
from .views.users import *
from .views.stories import *
from .views.chapters import *
from .views.signups import *
from .views.logins import *
from .views.reads import *
from .views.index import ApiIndex


urlpatterns = (
    # /api/
    url(r'^$', ApiIndex.as_view(), name='index'),

    # /users/
    url(r'^users$', UserIndex.as_view(), name='users-index'),

    # /users/1234
    url(r'^users/(?P<user_id>[0-9]+)/$', UserData.as_view(), name='user-details'),

    # /story/
    url(r'^stories$', StoryIndex.as_view(), name='story-index'),

    # /story/latest
    url(r'^stories/latest/$', StoryLatest.as_view(), name='story-latest'),

    # /story/1234/chapters
    url(r'^chapters$', ChaptersIndex.as_view(), name='chapters-index'),

    # /stats/signups
    url(r'^stats/signups/$', SignupView.as_view(), name='stats-signup'),

    # /stats/logins
    url(r'^stats/logins/$', LoginView.as_view(), name='stats-login'),

    # /stats/reads
    url(r'^stats/reads/$', ReadView.as_view(), name='stats-read'),
)
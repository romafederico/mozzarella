from rest_framework.views import APIView
from rest_framework.response import Response
from api.models import *
from api.serializers import *


class ReadView(APIView):

    def post(self, request):
        serializer = ReadsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "Read Logged"})
        else:
            data = {
                "error": True,
                "errors": serializer.errors,
            }
            return Response(data)

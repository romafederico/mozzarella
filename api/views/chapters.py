from rest_framework.views import APIView
from rest_framework.response import Response
from api.models.chapters import Chapter
from api.serializers import *


class ChaptersIndex(APIView):

    def get(self, request):
        story_id = self.request.query_params.get('story_id', None)
        if story_id:
            chapters = Chapter.objects.filter(story_id=story_id)
        serializer = ChaptersSerializer(chapters, many=True)
        return Response(serializer.data, headers={'Access-Control-Allow-Origin': '*'})

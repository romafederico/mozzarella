from rest_framework.views import APIView
from rest_framework.response import Response
from api.models import *
from api.serializers import *


class LoginView(APIView):

    def post(self, request):
        serializer = LoginsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "Login Logged"})
        else:
            data = {
                "error": True,
                "errors": serializer.errors,
            }
            return Response(data)

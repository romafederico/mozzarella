from rest_framework.views import APIView
from rest_framework.response import Response
from api.models import *
from api.serializers import *


class UserIndex(APIView):

    def get(self, request):
        user_id = self.request.query_params.get('user_id', None)
        email = self.request.query_params.get('email', None)
        status = self.request.query_params.get('status', None)
        if user_id:
            users = User.objects.filter(user_id=user_id)
        elif email:
            users = User.objects.filter(email=email)
        elif status:
            users = User.objects.filter(status=status)
        serializer = UsersSerializer(users, many=True)
        return Response(serializer.data, headers={'Access-Control-Allow-Origin': '*'})

    def post(self, request):
        print(request.data)
        serializer = UsersSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            data = {
                "error": True,
                "errors": serializer.errors,
            }
            return Response(data)


class UserData(APIView):

    def put(self, request, user_id):
        user = User.objects.get(user_id=user_id)
        serializer = UsersSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "User updated"})
        else:
            data = {
                "error": True,
                "errors": serializer.errors,
            }
            return Response(data)

from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from api.models.stories import Story
from api.models.chapters import Chapter
from api.serializers import *


class StoryIndex(APIView):

    def get(self, request):
        story_id = self.request.query_params.get('story_id', None)
        user_id = self.request.query_params.get('user_id', None)
        status = self.request.query_params.get('status', None)
        if story_id:
            stories = Story.objects.filter(story_id=story_id)
        elif user_id:
            stories = Story.objects.filter(user_id=user_id)
        elif status:
            stories = Story.objects.filter(status=status)
        serializer = StorySerializer(stories, many=True)
        return Response(serializer.data, headers={'Access-Control-Allow-Origin': '*'})


class StoryLatest(ListAPIView):
    serializer_class = StorySerializer

    def get(self, request):
        stories = Story.objects.all().filter(status=2).order_by('-creation_date')[:20]
        serializer = StorySerializer(stories, many=True)
        return Response(serializer.data)




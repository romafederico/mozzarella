# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class NmwAnalyticCompra(models.Model):
    idanalyticcompra = models.AutoField(db_column='IdAnalyticCompra', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    idanalyticplan = models.ForeignKey('NmwAnalyticPlan', models.DO_NOTHING, db_column='IdAnalyticPlan')  # Field name made lowercase.
    usuarios = models.IntegerField()
    dias = models.IntegerField()
    fecha = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'NMW_Analytic_Compra'


class NmwAnalyticPlan(models.Model):
    idanalyticplan = models.AutoField(db_column='IdAnalyticPlan', primary_key=True)  # Field name made lowercase.
    costo = models.IntegerField()
    dias = models.IntegerField()
    usuarios = models.IntegerField()
    detalle = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'NMW_Analytic_Plan'


class NmwAnalyticUsuarioAgregado(models.Model):
    idanalyticusuarioagregado = models.AutoField(db_column='IdAnalyticUsuarioAgregado', primary_key=True)  # Field name made lowercase.
    idanalyticcompra = models.ForeignKey(NmwAnalyticCompra, models.DO_NOTHING, db_column='IdAnalyticCompra')  # Field name made lowercase.
    idusuariowebagregado = models.IntegerField(db_column='IdUsuarioWebAgregado')  # Field name made lowercase.
    activo = models.IntegerField()
    fecha = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'NMW_Analytic_Usuario_Agregado'


class NmwArticulos(models.Model):
    idarticulo = models.AutoField(db_column='IdArticulo', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Articulos'


class NmwBic(models.Model):
    codigo = models.CharField(primary_key=True, max_length=50)
    nivel = models.IntegerField()
    padre = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NMW_Bic'


class NmwBicLang(models.Model):
    codigo = models.CharField(primary_key=True, max_length=50)
    lang = models.CharField(max_length=5)
    materia = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'NMW_Bic_lang'
        unique_together = (('codigo', 'lang'),)


class NmwComprasPapyre(models.Model):
    idcompra = models.AutoField(db_column='IdCompra', primary_key=True)  # Field name made lowercase.
    fechahora = models.DateTimeField(db_column='FechaHora', blank=True, null=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', blank=True, null=True)  # Field name made lowercase.
    emailpaypal = models.CharField(db_column='EmailPaypal', max_length=255, blank=True, null=True)  # Field name made lowercase.
    montoabonado = models.CharField(db_column='MontoAbonado', max_length=255, blank=True, null=True)  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=255, blank=True, null=True)  # Field name made lowercase.
    txn_id = models.CharField(max_length=255, blank=True, null=True)
    modelocomprado = models.CharField(db_column='ModeloComprado', max_length=255, blank=True, null=True)  # Field name made lowercase.
    creditosotorgar = models.IntegerField(db_column='CreditosOtorgar', blank=True, null=True)  # Field name made lowercase.
    datosenvio = models.TextField(db_column='DatosEnvio', blank=True, null=True)  # Field name made lowercase.
    error = models.CharField(db_column='Error', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Compras_Papyre'


class NmwContenidos(models.Model):
    story_id = models.AutoField(db_column='story_id', primary_key=True)  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    bic = models.CharField(max_length=50, blank=True, null=True)
    tipocontenido = models.IntegerField(db_column='TipoContenido')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150)  # Field name made lowercase.
    resumen = models.CharField(db_column='Resumen', max_length=3000)  # Field name made lowercase.
    palabrasclave = models.CharField(db_column='PalabrasClave', max_length=255, blank=True, null=True)  # Field name made lowercase.
    prologo = models.TextField(db_column='Prologo', blank=True, null=True)  # Field name made lowercase.
    imagen = models.CharField(db_column='Imagen', max_length=150, blank=True, null=True)  # Field name made lowercase.
    autor = models.CharField(db_column='Autor', max_length=150)  # Field name made lowercase.
    fechacreacion = models.DateField(db_column='FechaCreacion')  # Field name made lowercase.
    idgenero = models.IntegerField(db_column='IdGenero')  # Field name made lowercase.
    contadorleido = models.IntegerField(db_column='ContadorLeido')  # Field name made lowercase.
    contadordescargaspdf = models.IntegerField(db_column='ContadorDescargasPDF')  # Field name made lowercase.
    estado = models.IntegerField(db_column='Estado', blank=True, null=True)  # Field name made lowercase.
    cantidadpalabras = models.IntegerField(db_column='CantidadPalabras', blank=True, null=True)  # Field name made lowercase.
    borrador = models.IntegerField(db_column='Borrador', blank=True, null=True)  # Field name made lowercase.
    contadorestrellas1 = models.IntegerField(db_column='contadorEstrellas1', blank=True, null=True)  # Field name made lowercase.
    contadorestrellas2 = models.IntegerField(db_column='contadorEstrellas2', blank=True, null=True)  # Field name made lowercase.
    contadorestrellas3 = models.IntegerField(db_column='contadorEstrellas3', blank=True, null=True)  # Field name made lowercase.
    contadorestrellas4 = models.IntegerField(db_column='contadorEstrellas4', blank=True, null=True)  # Field name made lowercase.
    contadorestrellas5 = models.IntegerField(db_column='contadorEstrellas5', blank=True, null=True)  # Field name made lowercase.
    creditos = models.IntegerField(db_column='Creditos')  # Field name made lowercase.
    restringido = models.IntegerField(db_column='Restringido')  # Field name made lowercase.
    lang = models.CharField(max_length=5)
    concurso = models.IntegerField()
    isbn = models.CharField(db_column='ISBN', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ficcion = models.IntegerField(db_column='Ficcion', blank=True, null=True)  # Field name made lowercase.
    proteccion = models.IntegerField(db_column='Proteccion', blank=True, null=True)  # Field name made lowercase.
    licencia = models.CharField(db_column='Licencia', max_length=100, blank=True, null=True)  # Field name made lowercase.
    donacion = models.IntegerField(db_column='Donacion', blank=True, null=True)  # Field name made lowercase.
    paywtweet = models.IntegerField(db_column='PayWTweet', blank=True, null=True)  # Field name made lowercase.
    origen = models.CharField(db_column='Origen', max_length=2)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos'


class NmwContenidosCapitulos(models.Model):
    idcapitulo = models.AutoField(db_column='IdCapitulo', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    nrocapitulo = models.IntegerField(db_column='NroCapitulo')  # Field name made lowercase.
    publico = models.IntegerField(db_column='Publico')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150, blank=True, null=True)  # Field name made lowercase.
    exid = models.IntegerField(db_column='ExId', blank=True, null=True)  # Field name made lowercase.
    html = models.IntegerField()
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.
    cantidadpalabrascapitulo = models.IntegerField(db_column='CantidadPalabrasCapitulo', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos_Capitulos'


class NmwContenidosComentarios(models.Model):
    idcomentario = models.AutoField(db_column='IdComentario', primary_key=True)  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150)  # Field name made lowercase.
    texto = models.TextField(db_column='Texto')  # Field name made lowercase.
    estado = models.IntegerField(db_column='Estado')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos_Comentarios'


class NmwContenidosImprenta(models.Model):
    story_id = models.IntegerField(db_column='story_id', primary_key=True)  # Field name made lowercase.
    resumen = models.CharField(db_column='Resumen', max_length=1000)  # Field name made lowercase.
    biografia = models.TextField(db_column='Biografia')  # Field name made lowercase.
    story_idimprentaestado = models.ForeignKey('NmwContenidosImprentaEstado', models.DO_NOTHING, db_column='story_idImprentaEstado')  # Field name made lowercase.
    creditosimprenta = models.IntegerField(db_column='CreditosImprenta')  # Field name made lowercase.
    fecha = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos_Imprenta'


class NmwContenidosImprentaEstado(models.Model):
    story_idimprentaestado = models.AutoField(db_column='story_idImprentaEstado', primary_key=True)  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=100)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=1000)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos_Imprenta_Estado'


class NmwContenidosPromo(models.Model):
    idpromo = models.AutoField(db_column='IdPromo', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    montooriginal = models.IntegerField(db_column='MontoOriginal')  # Field name made lowercase.
    saldo = models.IntegerField(db_column='Saldo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos_Promo'


class NmwContenidosQuieroleer(models.Model):
    story_id = models.IntegerField(db_column='story_id', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Contenidos_QuieroLeer'
        unique_together = (('story_id', 'idusuarioweb'),)


class NmwCreditosCompras(models.Model):
    idcompra = models.AutoField(db_column='IdCompra', primary_key=True)  # Field name made lowercase.
    fechahora = models.DateTimeField(db_column='FechaHora')  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    creditos = models.IntegerField(db_column='Creditos')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Creditos_Compras'


class NmwCreditosOperacioneslog(models.Model):
    idlog = models.AutoField(db_column='IdLog', primary_key=True)  # Field name made lowercase.
    fechahora = models.DateTimeField(db_column='FechaHora')  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id', blank=True, null=True)  # Field name made lowercase.
    textolog = models.TextField(db_column='TextoLog', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Creditos_OperacionesLog'


class NmwCreditosPedidoscanje(models.Model):
    idpedido = models.AutoField(db_column='IdPedido', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    creditoscanje = models.IntegerField(db_column='CreditosCanje')  # Field name made lowercase.
    mensaje = models.TextField(db_column='Mensaje', blank=True, null=True)  # Field name made lowercase.
    fecharespuesta = models.DateTimeField(db_column='FechaRespuesta')  # Field name made lowercase.
    mensajerespuesta = models.TextField(db_column='MensajeRespuesta', blank=True, null=True)  # Field name made lowercase.
    estado = models.IntegerField(db_column='Estado')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Creditos_PedidosCanje'


class NmwCuentos(models.Model):
    idcuento = models.AutoField(db_column='IdCuento', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Cuentos'


class NmwDescargas(models.Model):
    iddescarga = models.AutoField(db_column='IdDescarga', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    idformatosebook = models.ForeignKey('NmwFormatosebook', models.DO_NOTHING, db_column='IdFormatosEbook')  # Field name made lowercase.
    fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NMW_Descargas'


class NmwEnsayos(models.Model):
    idensayo = models.AutoField(db_column='IdEnsayo', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Ensayos'


class NmwFormatosebook(models.Model):
    idformatosebook = models.AutoField(db_column='IdFormatosEbook', primary_key=True)  # Field name made lowercase.
    detalle = models.CharField(db_column='Detalle', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_FormatosEbook'


class NmwGeneros(models.Model):
    idgenero = models.AutoField(db_column='IdGenero', primary_key=True)  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    titulo_es = models.CharField(db_column='Titulo_es', max_length=150)  # Field name made lowercase.
    titulo_en = models.CharField(db_column='Titulo_en', max_length=150)  # Field name made lowercase.
    titulo_fr = models.CharField(db_column='Titulo_fr', max_length=150)  # Field name made lowercase.
    titulo_pt = models.CharField(db_column='Titulo_pt', max_length=150)  # Field name made lowercase.
    descripcion_es = models.TextField(db_column='Descripcion_es')  # Field name made lowercase.
    descripcion_en = models.TextField(db_column='Descripcion_en')  # Field name made lowercase.
    descripcion_fr = models.TextField(db_column='Descripcion_fr')  # Field name made lowercase.
    descripcion_pt = models.TextField(db_column='Descripcion_pt')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Generos'


class NmwGrupos(models.Model):
    idgrupo = models.AutoField(db_column='IdGrupo', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150)  # Field name made lowercase.
    descripcion = models.TextField(db_column='Descripcion')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Grupos'


class NmwGruposAmigos(models.Model):
    idgrupo = models.IntegerField(db_column='IdGrupo', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Grupos_Amigos'
        unique_together = (('idgrupo', 'idusuarioweb'),)


class NmwImpresiones(models.Model):
    idimpresion = models.AutoField(db_column='IdImpresion', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    fechaimpresion = models.DateTimeField(db_column='FechaImpresion')  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=40)  # Field name made lowercase.
    apellido = models.CharField(db_column='Apellido', max_length=40)  # Field name made lowercase.
    pais = models.CharField(db_column='Pais', max_length=100)  # Field name made lowercase.
    ciudad = models.CharField(db_column='Ciudad', max_length=200)  # Field name made lowercase.
    provinciaestado = models.CharField(db_column='ProvinciaEstado', max_length=200)  # Field name made lowercase.
    domicilio = models.CharField(db_column='Domicilio', max_length=200)  # Field name made lowercase.
    codigopostal = models.CharField(db_column='CodigoPostal', max_length=50)  # Field name made lowercase.
    fechanacimiento = models.DateField(db_column='FechaNacimiento')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Impresiones'


class NmwIncentivos(models.Model):
    idincentivo = models.AutoField(db_column='IdIncentivo', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id', blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateTimeField(db_column='Fecha', blank=True, null=True)  # Field name made lowercase.
    creditos = models.SmallIntegerField(db_column='Creditos', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(db_column='IP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    referencia = models.CharField(db_column='Referencia', max_length=200, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Incentivos'


class NmwLecturas(models.Model):
    idlectura = models.AutoField(db_column='IdLectura', primary_key=True)  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', blank=True, null=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    refererinicio = models.CharField(db_column='refererInicio', max_length=200, blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NMW_Lecturas'


class NmwLibros(models.Model):
    idlibro = models.AutoField(db_column='IdLibro', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    nrocapitulo = models.IntegerField(db_column='NroCapitulo')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150, blank=True, null=True)  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Libros'


class NmwMensajesAmigos(models.Model):
    idmensaje = models.AutoField(db_column='IdMensaje', primary_key=True)  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    idusuariowebamigo = models.IntegerField(db_column='IdUsuarioWebAmigo')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150)  # Field name made lowercase.
    texto = models.TextField(db_column='Texto')  # Field name made lowercase.
    leido = models.IntegerField(db_column='Leido')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Mensajes_Amigos'


class NmwMonedas(models.Model):
    origen = models.CharField(db_column='Origen', primary_key=True, max_length=5)  # Field name made lowercase.
    destino = models.CharField(db_column='Destino', max_length=5)  # Field name made lowercase.
    simbolo = models.CharField(max_length=20, blank=True, null=True)
    ratio = models.DecimalField(max_digits=10, decimal_places=4)
    fechaactualizacion = models.DateField(db_column='fechaActualizacion')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Monedas'
        unique_together = (('origen', 'destino'),)


class NmwMonografias(models.Model):
    idmonografia = models.AutoField(db_column='IdMonografia', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    nrocapitulo = models.IntegerField(db_column='NroCapitulo')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150, blank=True, null=True)  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Monografias'


class NmwNotificaciones(models.Model):
    idnotificaciones = models.AutoField(db_column='IdNotificaciones', primary_key=True)  # Field name made lowercase.
    idnotificacionestipo = models.ForeignKey('NmwNotificacionesTipo', models.DO_NOTHING, db_column='IdNotificacionesTipo')  # Field name made lowercase.
    keylang = models.CharField(db_column='KeyLang', max_length=100)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Notificaciones'


class NmwNotificacionesTipo(models.Model):
    idnotificacionestipo = models.AutoField(db_column='IdNotificacionesTipo', primary_key=True)  # Field name made lowercase.
    keylang = models.CharField(db_column='KeyLang', max_length=100)  # Field name made lowercase.
    detalle = models.CharField(db_column='Detalle', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Notificaciones_Tipo'


class NmwNovelas(models.Model):
    idnovela = models.AutoField(db_column='IdNovela', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    nrocapitulo = models.IntegerField(db_column='NroCapitulo')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150, blank=True, null=True)  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Novelas'


class NmwPais(models.Model):
    idpais = models.AutoField(db_column='IdPais', primary_key=True)  # Field name made lowercase.
    iso2 = models.CharField(db_column='ISO2', max_length=2)  # Field name made lowercase.
    idlang = models.CharField(db_column='IdLang', max_length=2)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=150)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Pais'


class NmwPaismoneda(models.Model):
    idpais = models.CharField(db_column='IdPais', primary_key=True, max_length=5)  # Field name made lowercase.
    idmoneda = models.CharField(db_column='IdMoneda', max_length=5)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_PaisMoneda'
        unique_together = (('idpais', 'idmoneda'),)


class NmwPaypalOperacionelog(models.Model):
    idlogpaypal = models.AutoField(db_column='IdLogPaypal', primary_key=True)  # Field name made lowercase.
    fechahora = models.DateTimeField(db_column='FechaHora', blank=True, null=True)  # Field name made lowercase.
    idusuarioweb = models.CharField(db_column='IdUsuarioWeb', max_length=255)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=255, blank=True, null=True)  # Field name made lowercase.
    monto = models.CharField(db_column='Monto', max_length=255, blank=True, null=True)  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tx = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NMW_PayPal_OperacioneLog'


class NmwPoemas(models.Model):
    idpoema = models.AutoField(db_column='IdPoema', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Poemas'


class NmwPromolog(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    fecha = models.DateTimeField(db_column='Fecha')  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    enstory_id = models.IntegerField(db_column='Enstory_id')  # Field name made lowercase.
    formato = models.CharField(db_column='Formato', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_PromoLog'


class NmwRegistracionLog(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    idcon = models.CharField(max_length=255, blank=True, null=True)
    fecha = models.DateTimeField()
    status = models.CharField(max_length=255)
    ip = models.CharField(max_length=255)
    notas = models.TextField()

    class Meta:
        managed = False
        db_table = 'NMW_Registracion_log'


class NmwSubastas(models.Model):
    idsubasta = models.AutoField(db_column='IdSubasta', primary_key=True)  # Field name made lowercase.
    tipo = models.CharField(max_length=2)
    fecha = models.DateField()
    horario = models.TimeField()
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    creditos = models.SmallIntegerField(db_column='Creditos')  # Field name made lowercase.
    story_idanterior = models.IntegerField(db_column='story_idAnterior', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Subastas'


class NmwSubastasLog(models.Model):
    idsubasta_log = models.AutoField(db_column='IdSubasta_log')  # Field name made lowercase.
    fechalog = models.DateTimeField()
    tipo = models.CharField(max_length=2)
    fecha = models.DateField()
    horario = models.TimeField()
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    creditos = models.SmallIntegerField(db_column='Creditos')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Subastas_log'


class NmwTesis(models.Model):
    idtesis = models.AutoField(db_column='IdTesis', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    nrocapitulo = models.IntegerField(db_column='NroCapitulo')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=150, blank=True, null=True)  # Field name made lowercase.
    contenido = models.TextField(db_column='Contenido', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Tesis'


class NmwUsuarios(models.Model):
    idusuario = models.AutoField(db_column='IdUsuario', primary_key=True)  # Field name made lowercase.
    usuario = models.CharField(db_column='Usuario', max_length=20)  # Field name made lowercase.
    clave = models.CharField(db_column='Clave', max_length=32)  # Field name made lowercase.
    nombrecompleto = models.CharField(db_column='NombreCompleto', max_length=80, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=150, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios'


class NmwUsuariosNotificaciones(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    idnotificaciones = models.ForeignKey(NmwNotificaciones, models.DO_NOTHING, db_column='IdNotificaciones')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_Notificaciones'
        unique_together = (('idusuarioweb', 'idnotificaciones'),)


class NmwUsuariosPadre(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    nombrepadre = models.CharField(db_column='NombrePadre', max_length=200)  # Field name made lowercase.
    dnipadre = models.CharField(db_column='DNIPadre', max_length=45, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_Padre'


class NmwUsuariosWeb(models.Model):
    idusuarioweb = models.AutoField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    origen = models.CharField(max_length=2)
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=150, blank=True, null=True)  # Field name made lowercase.
    nuevoemail = models.CharField(db_column='NuevoEmail', max_length=150)  # Field name made lowercase.
    clave = models.CharField(db_column='Clave', max_length=32)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=40)  # Field name made lowercase.
    apellido = models.CharField(db_column='Apellido', max_length=40)  # Field name made lowercase.
    pseudonimo = models.CharField(db_column='Pseudonimo', max_length=80)  # Field name made lowercase.
    pais = models.CharField(db_column='Pais', max_length=100)  # Field name made lowercase.
    ciudad = models.CharField(db_column='Ciudad', max_length=200)  # Field name made lowercase.
    provinciaestado = models.CharField(db_column='ProvinciaEstado', max_length=200)  # Field name made lowercase.
    domicilio = models.CharField(db_column='Domicilio', max_length=200)  # Field name made lowercase.
    codigopostal = models.CharField(db_column='CodigoPostal', max_length=50)  # Field name made lowercase.
    librocabecera = models.CharField(db_column='LibroCabecera', max_length=200)  # Field name made lowercase.
    influencias = models.CharField(db_column='Influencias', max_length=500)  # Field name made lowercase.
    web = models.CharField(db_column='Web', max_length=200)  # Field name made lowercase.
    telefono = models.CharField(db_column='Telefono', max_length=200)  # Field name made lowercase.
    dni = models.CharField(db_column='DNI', max_length=45, blank=True, null=True)  # Field name made lowercase.
    fechanacimiento = models.DateField(db_column='FechaNacimiento', blank=True, null=True)  # Field name made lowercase.
    biografia = models.TextField(db_column='Biografia', blank=True, null=True)  # Field name made lowercase.
    imagen = models.CharField(db_column='Imagen', max_length=150, blank=True, null=True)  # Field name made lowercase.
    estado = models.IntegerField(db_column='Estado')  # Field name made lowercase.
    creditos = models.IntegerField(db_column='Creditos')  # Field name made lowercase.
    notificacionescomentario = models.IntegerField(db_column='NotificacionesComentario')  # Field name made lowercase.
    notificacionesdescarga = models.IntegerField(db_column='NotificacionesDescarga')  # Field name made lowercase.
    notificacionesbiblioteca = models.IntegerField(db_column='NotificacionesBiblioteca')  # Field name made lowercase.
    notificacionesamigo = models.IntegerField(db_column='NotificacionesAmigo')  # Field name made lowercase.
    notificacionesmensaje = models.IntegerField(db_column='NotificacionesMensaje')  # Field name made lowercase.
    notificacionescomentariootros = models.IntegerField(db_column='NotificacionesComentarioOtros')  # Field name made lowercase.
    lang = models.CharField(max_length=5)
    idpais = models.IntegerField(db_column='IdPais', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_Web'


class NmwUsuariosWebAmigos(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    idusuariowebamigo = models.IntegerField(db_column='IdUsuarioWebAmigo')  # Field name made lowercase.
    fechaalta = models.DateTimeField(db_column='FechaAlta')  # Field name made lowercase.
    estado = models.IntegerField(db_column='Estado')  # Field name made lowercase.
    notificado = models.IntegerField(db_column='Notificado')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_Web_Amigos'
        unique_together = (('idusuarioweb', 'idusuariowebamigo'),)


class NmwUsuariosWebLogins(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb')  # Field name made lowercase.
    fecha = models.DateTimeField(blank=True, null=True)
    ip = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_Web_Logins'


class NmwUsuariosWebPuntajes(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    pfa = models.FloatField(db_column='PFA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_Web_Puntajes'


class NmwUsuariosFb(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    id = models.BigIntegerField(unique=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    picture = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NMW_Usuarios_fb'


class NmwVotos(models.Model):
    idusuarioweb = models.IntegerField(db_column='IdUsuarioWeb', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    valor = models.IntegerField()
    fecha = models.DateTimeField(blank=True, null=True)
    ip = models.CharField(max_length=255)
    notas = models.TextField()

    class Meta:
        managed = False
        db_table = 'NMW_Votos'
        unique_together = (('idusuarioweb', 'story_id'),)


class OnxCatalogoHistorial(models.Model):
    idhistorial = models.AutoField(db_column='IdHistorial', primary_key=True)  # Field name made lowercase.
    registrosprocesados = models.IntegerField(db_column='RegistrosProcesados')  # Field name made lowercase.
    actualizaciones = models.IntegerField(db_column='Actualizaciones')  # Field name made lowercase.
    altas = models.IntegerField(db_column='Altas')  # Field name made lowercase.
    logerrores = models.TextField(db_column='LogErrores')  # Field name made lowercase.
    fechainicio = models.DateTimeField(db_column='FechaInicio')  # Field name made lowercase.
    fechafin = models.DateTimeField(db_column='FechaFin')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Catalogo_Historial'


class OnxColeccion(models.Model):
    idcoleccion = models.AutoField(db_column='IdColeccion', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Coleccion'


class OnxColeccionidentificador(models.Model):
    ididentificador = models.AutoField(db_column='IdIdentificador', primary_key=True)  # Field name made lowercase.
    idcoleccion = models.IntegerField(db_column='IdColeccion')  # Field name made lowercase.
    tipoidcoleccion = models.IntegerField(db_column='TipoIdColeccion')  # Field name made lowercase.
    idvalor = models.CharField(db_column='IdValor', max_length=30)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_ColeccionIdentificador'


class OnxConstraints(models.Model):
    idconstraint = models.AutoField(db_column='IdConstraint', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    estado = models.IntegerField(db_column='Estado')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Constraints'


class OnxContenidos(models.Model):
    story_idonix = models.AutoField(db_column='story_idOnix', primary_key=True)  # Field name made lowercase.
    story_id = models.IntegerField(db_column='story_id')  # Field name made lowercase.
    idreferencia = models.CharField(db_column='IdReferencia', max_length=20)  # Field name made lowercase.
    tiponotificacion = models.IntegerField(db_column='TipoNotificacion')  # Field name made lowercase.
    composicion = models.IntegerField(db_column='Composicion')  # Field name made lowercase.
    forma = models.CharField(db_column='Forma', max_length=10)  # Field name made lowercase.
    formadetalle = models.CharField(db_column='FormaDetalle', max_length=10)  # Field name made lowercase.
    epubtecnicaproteccion = models.IntegerField(db_column='EpubTecnicaProteccion')  # Field name made lowercase.
    titulotipo = models.IntegerField(db_column='TituloTipo')  # Field name made lowercase.
    titulonivel = models.IntegerField(db_column='TituloNivel')  # Field name made lowercase.
    subtitulo = models.CharField(db_column='SubTitulo', max_length=150)  # Field name made lowercase.
    idcontribuyente = models.IntegerField(db_column='IdContribuyente')  # Field name made lowercase.
    idiomarol = models.IntegerField(db_column='IdiomaRol')  # Field name made lowercase.
    idiomacodigo = models.CharField(db_column='IdiomaCodigo', max_length=5)  # Field name made lowercase.
    numeropaginas = models.IntegerField(db_column='NumeroPaginas')  # Field name made lowercase.
    paispublicacion = models.CharField(db_column='PaisPublicacion', max_length=10)  # Field name made lowercase.
    ideditor = models.IntegerField(db_column='IdEditor')  # Field name made lowercase.
    estadopublicacion = models.IntegerField(db_column='EstadoPublicacion')  # Field name made lowercase.
    fechapublicacionrol = models.IntegerField(db_column='FechaPublicacionRol')  # Field name made lowercase.
    fechapublicacionformato = models.CharField(db_column='FechaPublicacionFormato', max_length=20)  # Field name made lowercase.
    derechosdeventatipo = models.IntegerField(db_column='DerechosDeVentaTipo')  # Field name made lowercase.
    derechosdeventapaises = models.CharField(db_column='DerechosDeVentaPaises', max_length=1000)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Contenidos'


class OnxContribuyente(models.Model):
    idcontribuyente = models.AutoField(db_column='IdContribuyente', primary_key=True)  # Field name made lowercase.
    rol = models.CharField(db_column='Rol', max_length=20)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Contribuyente'


class OnxEditor(models.Model):
    ideditor = models.AutoField(db_column='IdEditor', primary_key=True)  # Field name made lowercase.
    rol = models.IntegerField(db_column='Rol')  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Editor'


class OnxGrado(models.Model):
    idgrado = models.AutoField(db_column='IdGrado', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    valor = models.IntegerField(db_column='Valor')  # Field name made lowercase.
    unidad = models.IntegerField(db_column='Unidad')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Grado'


class OnxIdentificador(models.Model):
    ididentificador = models.AutoField(db_column='IdIdentificador', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipoidproducto = models.IntegerField(db_column='TipoIdProducto')  # Field name made lowercase.
    idvalor = models.CharField(db_column='IdValor', max_length=30)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Identificador'


class OnxImprenta(models.Model):
    idimprenta = models.AutoField(db_column='IdImprenta', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    valor = models.CharField(db_column='Valor', max_length=200)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Imprenta'


class OnxLang(models.Model):
    idlang = models.AutoField(db_column='IdLang', primary_key=True)  # Field name made lowercase.
    langiso6392 = models.CharField(db_column='LangISO6392', max_length=5)  # Field name made lowercase.
    langiso6391 = models.CharField(db_column='LangISO6391', max_length=5)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Lang'


class OnxMaterialrelacionado(models.Model):
    idrelacionado = models.AutoField(db_column='IdRelacionado', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    codigo = models.IntegerField(db_column='Codigo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_MaterialRelacionado'


class OnxMaterialrelacionadoidentificador(models.Model):
    ididentificador = models.AutoField(db_column='IdIdentificador', primary_key=True)  # Field name made lowercase.
    idrelacionado = models.IntegerField(db_column='IdRelacionado')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=100)  # Field name made lowercase.
    valor = models.CharField(db_column='Valor', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_MaterialRelacionadoIdentificador'


class OnxMercado(models.Model):
    idmercado = models.AutoField(db_column='IdMercado', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    territorio = models.CharField(db_column='Territorio', max_length=1000)  # Field name made lowercase.
    estadopublicacion = models.IntegerField(db_column='EstadoPublicacion')  # Field name made lowercase.
    fechapublicacionrol = models.IntegerField(db_column='FechaPublicacionRol')  # Field name made lowercase.
    fechapublicacionformato = models.CharField(db_column='FechaPublicacionFormato', max_length=20)  # Field name made lowercase.
    fechapublicacion = models.DateTimeField(db_column='FechaPublicacion')  # Field name made lowercase.
    proveedor = models.CharField(db_column='Proveedor', max_length=200)  # Field name made lowercase.
    proveedorrol = models.IntegerField(db_column='ProveedorRol')  # Field name made lowercase.
    disponibilidad = models.IntegerField(db_column='Disponibilidad')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Mercado'


class OnxPrecio(models.Model):
    idprecio = models.AutoField(db_column='IdPrecio', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    cantidad = models.FloatField(db_column='Cantidad')  # Field name made lowercase.
    monedacodigo = models.CharField(db_column='MonedaCodigo', max_length=10)  # Field name made lowercase.
    territorio = models.CharField(db_column='Territorio', max_length=1000)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Precio'


class OnxRecurso(models.Model):
    idrecurso = models.AutoField(db_column='IdRecurso', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    audiencia = models.IntegerField(db_column='Audiencia')  # Field name made lowercase.
    modo = models.IntegerField(db_column='Modo')  # Field name made lowercase.
    forma = models.IntegerField(db_column='Forma')  # Field name made lowercase.
    link = models.CharField(db_column='Link', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Recurso'


class OnxTema(models.Model):
    idtema = models.AutoField(db_column='IdTema', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    esquemaidentificador = models.IntegerField(db_column='EsquemaIdentificador')  # Field name made lowercase.
    codigo = models.CharField(db_column='Codigo', max_length=10)  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Tema'


class OnxTexto(models.Model):
    idtexto = models.AutoField(db_column='IdTexto', primary_key=True)  # Field name made lowercase.
    story_idonix = models.IntegerField(db_column='story_idOnix')  # Field name made lowercase.
    tipo = models.IntegerField(db_column='Tipo')  # Field name made lowercase.
    audiencia = models.IntegerField(db_column='Audiencia')  # Field name made lowercase.
    idiomacodigo = models.CharField(db_column='IdiomaCodigo', max_length=10)  # Field name made lowercase.
    texto = models.TextField(db_column='Texto')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ONX_Texto'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Etario(models.Model):
    idusuario = models.IntegerField(blank=True, null=True)
    anio = models.IntegerField(blank=True, null=True)
    categoria = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'etario'
